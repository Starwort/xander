import asyncio
import datetime
import json
import aiohttp
import discord
from discord.ext import commands
import motor.motor_asyncio
import traceback
import config
import sys

def get_prefix(bot, message):
    """A callable Prefix for our bot. This could be edited to allow per server prefixes."""
    
    # Notice how you can use spaces in prefixes. Try to keep them simple though.
    prefixes = ['>']

    # If we are in a guild, we allow for the user to mention us or use any of the prefixes in our list.
    return commands.when_mentioned_or(*prefixes)(bot, message)

# Below cogs represents our folder our cogs are in. Following is the file name. So 'meme.py' in cogs, would be cogs.meme
# Think of it like a dot path import
initial_extensions = [
    'cogs.dl'

]   
bot = commands.Bot(command_prefix=get_prefix,owner_id=124316478978785283)

@bot.event
async def on_ready():
    print('Logged in as: '+ bot.user.name)
    print ('ID: ' +str(bot.user.id))
    print('------')
    if __name__ == '__main__':
        for extension in initial_extensions:
            try:
                bot.load_extension(extension)
            except Exception as e:
                print('Failed to load extension'+str(extension))
                traceback.print_exc()


@bot.command()
async def ping(ctx):
    png = str(round(bot.latency * 1000,2))
    return await ctx.send("Pong! `"+png+'ms` :ping_pong:')

#########################################################  BELOW THIS LINE IS INIT ######################################################### 
try:

    bot.run(config.TOKEN, bot=True)

except Exception as e:
    for task in asyncio.Task.all_tasks():
        task.cancel()
    traceback.print_exc()
    print(e)
    print('[@] ERROR')
    sys.exit(-1)
